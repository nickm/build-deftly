# `build-deftly`

This crate is a project to test the `derive-deftly` macro system
by attempting to clone the `derive_builder` crate.

As it stands, I think I have gotten an implementation for every feature that
`derive_builder` has, but in some cases I had to give them a different syntax.
I have not tested every combination of features; probably some combinations
will break.

## Converting to use this crate.

Instead of this...

```rust,ignore
use derive_builder::Builder;
#[derive(Builder)]
struct MyStruct {
   ...
}
```

Say this:

```rust,ignore
use derive_deftly::Deftly;
use build_deftly::templates::*;
#[derive(Deftly)]
#[derive_deftly(Builder)]
```

If you use any `#[builder(...)]` options,
you will have to change them to `#[deftly(builder(...))]` instead.

Some options have a different form; see below.

## Supported options

So far, these options are supported.
(Remember, they go inside `#[deftly(builder(...))]`.)

 * `pattern = "PAT"` — Supported on fields and structs.
   `PAT` can be `immutable`, `owned`, or `mutable` (the default).

 * `setter(skip)`, `setter(custom)`  — Supported on fields and structs.
   Note that we do _not_ support `skip=true`, `skip=false`,
   `custom=true`, or `custom=false`.
   Instead of `skip=true`, say `skip`.
   Instead of `custom=true`, say `custom`.
   Instead of `skip=false` or `custom=false`, say "enabled",
   as in `setter(enabled)`.
   (See derive-deftly#40, derive-deftly#48, derive-deftly#49.)

 * `vis = "VISIBILITY"`  — Supported on fields and structs.
   By default, builders and setters have the same visibility
   as the original struct.
   To override this on the builder and its methods,
   say (for example) `vis="pub"` or `vis="pub(crate)`.
   This replaces `derive_builder`'s `public` and `private` options.

 * `name = "NAME"` — Supported on structs.
   Renames the generated builder structure and error enum.

 * `setter(prefix = "PREFIX")` — Supported on fields and structs.
   Adds a prefix (with an underscore) to setter methods.

 * `setter(name = "PREFIX")` — Supported on fields.
   Rename a single setter method.

 * `setter(into)` — Supported on fields.
   Makes a setter generic over `V:Into<..>`.

 * `setter(try_into)` — Supported on fields.
   Makes a setter generic over `V:TryInto<..>`.

 * `setter(try_into)` — Supported on fields of type `Option<T>`.
   Makes a setter take a `T` instead of an `Option<T>`.
   (When used with `into`, makes the setter take `impl Into<T>`
   instead of `impl Into<Option<T>>`;
   behaves analogously with try_into.)

 * `default`, `default_val = "EXPR"`  — Supported on fields.
   Unlike `derive_builder`, these are separate options,
   since `derive_deftly` doesn't support options with optional
   values.  (See see derive-deftly#40, derive-deftly#48.)

 * `build_fn(skip)` — Supported on structs.
   Skips generation of the `build()` function.

 * `build_fn(name = "NAME")` — Supported on structs.
   Provides a new name for the `build()` function.

 * `build_fn(validate = "PATH")` — Supported on structs.
   Provides a function to run before the rest of the builder,
   to validate its inputs.
   The function must take the Builder by reference,
   and return `Result<_, String>`.

 * `build_fn(error = "NAME")` — Supported on structs.
   Provides a user-supplied error type.  It must implement
   `From<UninitializedFieldError>`.

 * `derive = "TYPES"` — Supported on structs.
   Causes the generated Builder to derive the provided list of comma-separated types.
   Unlike derive_builder, this must be a string.
   (See derive-deftly#56.)

 * `struct_attr = "ATTRS"`, `impl_attr = "ATTRS"`  — Supported on structs.
   Declares one or more attributes that should be applied verbatim
   to the builder or to its `impl` block.
   These need to include the full attribute syntax, as in
   `struct_attr = "#[must_use]"`.
   (This differs from `derive_builder` in its name and syntax;
   see derive-deftly#56.)

 * `field_attr = "ATTRS"`, `setter_attr = "ATTRS"`  — Supported on fields.
   Declares one or more attributes that should be applied verbatim
   to the field in the builder, or to its setter function.
   These need to include the full attribute syntax, as in
   `field_attr = "#[serde(default)]")`.
   (This differs from `derive_builder` in its name and syntax;
   see derive-deftly#56.)

 * `field(ty)` — Supported on fields.
   Overrides the type of the field as stored in the builder.

 * `field(build_fn)`, `field(try_build_fn)` — Supported on fields.
   Overrides the code used to generate the field from the field
   stored in the builder.
   These options take an expression that must evaluate to a function on &self.
   The `build_fn` variant should return the type of the field as it appears in
   the underlying struct;
   The `try_build_fn` should return a `Result`.
   (This differs from `derive_builder` in its name and syntax;
   see derive-deftly#55.)

 * `sub_builder`, `sub_builder(fn_name)`  — Supported on fields.
   Behaves as in `derive-deftly-fork-arti`.
   (Doesn't work on generic types; see derive-deftly#59.)

## Additional features

We add a `Self::builder()` method to your original type
that returns an empty builder.
