use build_deftly::prelude::*;
use derive_deftly::Deftly;

#[test]
fn defaults() {
    #[derive(Deftly, Debug)]
    #[derive_deftly(Builder)]
    struct Small {
        #[deftly(builder(default_val = "my_string()"))]
        hello: String,
        #[deftly(builder(default))]
        world: u32,
    }

    fn my_string() -> String {
        "hello world".to_string()
    }

    let s = Small::builder().build().unwrap();
    assert_eq!(&s.hello, "hello world");
    assert_eq!(s.world, 0);

    let s = Small::builder()
        .hello("hi there".to_string())
        .world(7)
        .build()
        .unwrap();
    assert_eq!(&s.hello, "hi there");
    assert_eq!(s.world, 7);
}
